<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lv_for_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'tomcat');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Delq[Ls?og5:Dkxhp,]g^ym^rI4irc,|f4:lN4O3+y* DBt?QvR8zxWKAkwRj$A0');
define('SECURE_AUTH_KEY',  'T%e3:*<T8Bk@p+1jy!`kC=(O]pD*[$h Di<_b(mWEnA4ec0]3>}LJ*t?`-X^VVr]');
define('LOGGED_IN_KEY',    'cNL%W*$fMUFpX$!_Y5*_%zOZzCn09%jn p+CEIJ?i{Jkwyl_x6OZcUue:VJ$&eoN');
define('NONCE_KEY',        'ipq6zAsEn9w{l^]MyzKCm2XD5SLf(7Plr+AhrY8Jxg/~v.;mbFTG5.>Xbr28MRjI');
define('AUTH_SALT',        '9Is{<[c-7K-D7/h6|.p!mYvNPn6aJ&/4]dEpldS4@M5{1={|IZ@s2Tj+_FIUc{3w');
define('SECURE_AUTH_SALT', 'mA}>u:RsAx~{oHZk[-y7z_n(KKq.036,}qs-vcz/[z&d;G:_he^A4;qknRs5;9:n');
define('LOGGED_IN_SALT',   '}=lYY]P*(j[>:)9g:Cie)+I&OF/mF%TIu4pQ|ZJy%Gu2W1sHPqI;b--.Qo`HPB<D');
define('NONCE_SALT',       'c:~g;gZnjV.>GnD?5j-asr52.@#E.<1IJ3UiWd,EZOL)Q{B)KKoe9>T{USCr_n>i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
