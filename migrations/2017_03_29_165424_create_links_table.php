<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('link_id',20)->unsigned();
            $table->string('link_url',191)->default('');
            $table->string('link_name',191)->default('');
            $table->string('link_image',191)->default('');
            $table->string('link_target',25)->default('');
            $table->string('link_description',191)->default('');
            $table->string('link_visible',20)->default('')->index();
            $table->bigInteger('link_owner')->unsigned()->default(1);
            $table->integer('link_rating')->default(0);
            $table->dateTime('link_updated')->nullable();
            $table->string('link_rel',191)->default('');
            $table->mediumText('link_notes');
            $table->string('link_rss',191)->default('');
        });
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
