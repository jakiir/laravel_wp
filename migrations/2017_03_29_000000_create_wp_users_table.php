<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('ID',20)->unsigned();
            $table->string('user_login',60)->index()->default('');
            $table->string('user_pass',191)->default('');
            $table->string('user_nicename',50)->index()->default('');
            $table->string('user_email',100)->index()->default('');
            $table->string('user_url',100)->default('');
            $table->dateTime('user_registered')->nullable();
            $table->string('user_activation_key',191)->default('');
            $table->integer('user_status')->default(0);
            $table->string('display_name',191)->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
