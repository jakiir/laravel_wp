<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('comment_ID',20)->unsigned();
            $table->bigInteger('comment_post_ID')->unsigned()->default(1)->index();
            $table->string('comment_author',10);
            $table->string('comment_author_email',100)->default('')->index();
            $table->string('comment_author_url',191)->default('');
            $table->string('comment_author_IP',100)->default('');
            $table->dateTime('comment_date')->nullable();
            $table->dateTime('comment_date_gmt')->nullable()->index();
            $table->text('comment_content');
            $table->integer('comment_karma')->default(0);
            $table->string('comment_approved',20)->default(1)->index();
            $table->string('comment_agent',191)->default('');
            $table->string('comment_type',20)->default('');
            $table->bigInteger('comment_parent')->unsigned()->default(0)->index();
            $table->bigInteger('user_id')->unsigned()->default(0);
        });
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
