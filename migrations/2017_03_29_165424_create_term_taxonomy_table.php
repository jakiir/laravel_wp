<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermTaxonomyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_taxonomy', function (Blueprint $table) {
            $table->bigInteger('term_taxonomy_id')->unsigned();
            $table->bigInteger('term_id')->unique()->unsigned()->default(0);
            $table->string('taxonomy',32)->index()->default('');
            $table->longText('description');
            $table->bigInteger('parent')->default();
            $table->bigInteger('count')->default(0);
        });
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term_taxonomy');
    }
}
