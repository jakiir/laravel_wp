<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('ID',20)->unsigned();
            $table->bigInteger('post_author')->unsigned()->default(0)->index();
            $table->dateTime('post_date')->nullable();
            $table->dateTime('post_date_gmt')->nullable();            
            $table->longText('post_content');
            $table->text('post_title');
            $table->text('post_excerpt');
            $table->string('post_status',20)->default('publish');
            $table->string('comment_status',20)->default('open');
            $table->string('ping_status',20)->default('open');
            $table->string('post_password',191);
            $table->string('post_name',20)->index();
            $table->text('to_ping');
            $table->text('pinged');
            $table->dateTime('post_modified')->nullable();
            $table->dateTime('post_modified_gmt')->nullable();
            $table->longText('post_content_filtered'); 
            $table->bigInteger('post_parent')->unsigned()->default(0)->index();
            $table->string('guid',191)->default('');
            $table->integer('menu_order')->default(0);
            $table->string('post_type',20)->default('post')->index();
            $table->string('post_mime_type',100)->default('');
            $table->bigInteger('comment_count')->default(0);
        });
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
